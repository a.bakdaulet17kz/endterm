package main

import (
	"fmt"
	"gitlab.com/a.bakdaulet17kz/endterm/calculator/calculatorpb"
	"google.golang.org/grpc"
	"io"
	"log"
	"net"
	"time"
)

type Server struct {
	calculatorpb.UnimplementedCalculatorServiceServer
}

func (*Server) PrimeNumberDecomposition(req *calculatorpb.PrimeNumberDecompositionRequest, stream calculatorpb.CalculatorService_PrimeNumberDecompositionServer) error {
	fmt.Printf("PrimeNumberDecomposition function was invoked with %v \n", req)
	number := req.GetNumber()
	var factor int64 = 2

	for number > 1 {
		if number%factor == 0 {
			res := &calculatorpb.PrimeNumberDecompositionResponse{Result: factor}
			err := stream.Send(res)
			if err != nil {
				log.Fatalf("error while sending PrimeNumberDecomposition responses: %v", err.Error())
			}
			number = number / factor
			time.Sleep(time.Second)
		} else {
			factor++
		}
	}

	return nil
}

func (*Server) ComputeAverage(stream calculatorpb.CalculatorService_ComputeAverageServer) error {
	fmt.Printf("ComputeAverage function was invoked with a streaming request\n")

	var sum int64 = 0
	var count int64 = 0

	for {
		req, err := stream.Recv()
		if err == io.EOF {
			average := float64(sum) / float64(count)
			return stream.SendAndClose(&calculatorpb.ComputeAverageResponse{
				Result: average,
			})
		}
		if err != nil {
			log.Fatalf("Error while reading client stream: %v", err)
		}
		sum += req.GetNumber()
		count++
	}
}

func main() {
	l, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}
	s := grpc.NewServer()
	calculatorpb.RegisterCalculatorServiceServer(s, &Server{})
	log.Println("Server is running on port:50051")
	if err := s.Serve(l); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
